//
//  ViewController.swift
//  SnowDevilsFinal
//
//  Created by Rafael Zamora on 11/7/18.
//  Copyright © 2018 rafaelzamora. All rights reserved.
//

import UIKit
import FirebaseDatabase

struct JSON: Codable {
    var weather: String
    var dates: [String]
    var location: String
}

class TripTableViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    let cellID = "customTripCell"
    let apiURL = "https://api.openweathermap.org/data/2.5/weather"
    let token = "b6fd9f73c900fc2057d28122be90729a"
    var ref:DatabaseReference!
    var weatherDictionary = NSMutableDictionary()
    
    var trips : [Trip] = [Trip]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.ref = Database.database().reference()
        self.getWeather()
        sleep(1)
        self.createTrips()
        self.tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    func createTrips() {
        self.ref.child("trips").observeSingleEvent(of: .value, with: { (snapshot) in
            guard let data = snapshot.value as? [String: Any] else { return }
            
            for resort in data {
                var dates = [String]()
                var resortLocation = ""
                var resortCondtion = ""
                var resortName = ""
                resortName = resort.key
                guard let stuff = resort.value as? [String: Any] else { return }
                print(stuff)
                resortLocation = stuff["location"] as! String
                resortCondtion = self.weatherDictionary[resortLocation] as! String
                dates = stuff["dates"] as? NSArray as! [String]
                for date in dates {
                    print("\(resortLocation) : \(resortCondtion)")
                    self.trips.append(Trip(name: resortName, picture: UIImage(named: resortName)!,
                                           location: resortLocation, condition: resortCondtion, date: date))
                    self.tableView.reloadData()
                }
            }
            
        })
    }
    
    func getWeather() {
        let locationArray = ["flagstaff", "albuquerque", "payson", "durango"]
        var weather = ""
        let group = DispatchGroup()
        for city in locationArray {
            print("Making api call")
            group.enter()
            let endpoint = "\(apiURL)?q=\(city)&appid=\(token)"
            let url = URL(string: endpoint)!
            let urlSession = URLSession.shared
            let jsonQuery = urlSession.dataTask(with: url, completionHandler: { data, response, error -> Void in
                if (error != nil) {
                    print(error!.localizedDescription)
                } else {
                    let jsonResult = (try! JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)) as! NSDictionary
                    let weatherArray : NSArray = jsonResult["weather"] as! NSArray
                    let weatherDict : NSDictionary = weatherArray[0] as! NSDictionary
                    DispatchQueue.main.async(execute: {
                        weather = weatherDict["description"]! as! String
                        self.weatherDictionary[city] = weather
                        group.leave()
//                        print("\(city) : \(String(describing: self.weatherDictionary[city]!))")
                    })
                }
            })
            jsonQuery.resume()
        }
    }
}

// TableView Setup
extension TripTableViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.trips.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
            cell.contentView.backgroundColor = UIColor.clear
        let whiteRoundedView : UIView = UIView(frame: CGRect(origin: CGPoint(x: 0, y: 20), size: CGSize(width: cell.layer.borderWidth, height: 1)))
        whiteRoundedView.layer.backgroundColor = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [1.0, 1.0, 1.0, 1.0])
            whiteRoundedView.layer.masksToBounds = false
            whiteRoundedView.layer.cornerRadius = 5.0
            whiteRoundedView.layer.shadowOffset = CGSize(width: -1, height: 1)
            whiteRoundedView.layer.shadowOpacity = 0.5
            cell.contentView.addSubview(whiteRoundedView)
            cell.contentView.sendSubviewToBack(whiteRoundedView)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! CustomTripCell
        let trip = self.trips[indexPath.row]
        
        cell.setTrip(trip: trip)
        cell.layer.borderWidth = 1
        cell.layer.borderColor = UIColor.black.cgColor
        cell.layer.cornerRadius = 10
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}
