//
//  AddTripViewController.swift
//  SnowDevilsFinal
//
//  Created by Rafael Zamora on 11/10/18.
//  Copyright © 2018 rafaelzamora. All rights reserved.
//

import UIKit
import FirebaseDatabase

class Resort {
    var resortName : String
    var resortLogo : UIImage
    init(name: String, image: UIImage) {
        resortName = name
        resortLogo = image
    }
}

class AddTripViewController : UIViewController, UITextFieldDelegate {

    // Properties / Atrributes for the AddTripViewController
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var resortNameTextField: UITextField!
    @IBOutlet weak var availableSeats: UITextField!
    var ref:DatabaseReference!
    var resortArray = [Resort(name:"SnowBowl", image: UIImage(named: "SnowBowl")!),
                       Resort(name:"Sunrise", image: UIImage(named: "Sunrise")!),
                       Resort(name:"AngelFire", image: UIImage(named: "AngelFire")!),
                       Resort(name:"Whistler", image: UIImage(named: "Whistler")!)]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        availableSeats.delegate = self
    }
    
    @IBAction func saveTripToDatabase(_ sender: Any) {
        if (resortNameTextField.text != "") {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM/dd/yy"
            let dateString = dateFormatter.string(from: datePicker.date)
            var dates = [String]()
            self.ref.child("/trips/\(resortNameTextField.text!)/dates/").observeSingleEvent(of: .value, with: {(snapshot) in
//                print(snapshot.value as! NSArray)
                dates = (snapshot.value as! NSArray) as! [String]
                print(dates)
                dates.append(dateString)
                print(dates)
                let childUpdates = ["/trips/\(self.resortNameTextField.text!)/dates/": dates]
                self.ref.updateChildValues(childUpdates)
            })
            
        }
    }
    
    func writeUserData(data: NSDictionary) {
        self.ref.setValue(data)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        availableSeats.resignFirstResponder()
        return true
    }
}

extension AddTripViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.resortArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ResortTableViewCell", for: indexPath) as! ResortTableViewCell
        
        cell.resortImage.image = self.resortArray[indexPath.row].resortLogo
        cell.layer.cornerRadius = 5
        cell.clipsToBounds = true
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let resort = self.resortArray[indexPath.row]
        resortNameTextField.text = resort.resortName
        print(resort.resortName)
    }
    
    
}
