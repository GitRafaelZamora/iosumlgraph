//
//  Trips.swift
//  SnowDevilsFinal
//
//  Created by Rafael Zamora on 11/9/18.
//  Copyright © 2018 rafaelzamora. All rights reserved.
//

import UIKit

struct Trip {
    var name        : String
    var picture     : UIImage
    var location    : String
    var condition   : String
    var date        : String
}

