//
//  TopBar.swift
//  SnowDevilsFinal
//
//  Created by Rafael Zamora on 11/12/18.
//  Copyright © 2018 rafaelzamora. All rights reserved.
//

import UIKit

class TopBar: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func draw(_ rect: CGRect) {
        addBottomBorderWithColor(color: UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 1), width: self.frame.width)
    }
}

extension UIView {
    func addBottomBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
//        border.frame = CGRectMake(0, self.frame.size.height - width, self.frame.size.width, width)
        border.frame = CGRect(origin: CGPoint(x: 0, y: 40), size: CGSize(width: self.frame.width, height: 1))
        self.layer.addSublayer(border)
    }
}
