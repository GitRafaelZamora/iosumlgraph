//
//  TripCell.swift
//  SnowDevilsFinal
//
//  Created by Rafael Zamora on 11/10/18.
//  Copyright © 2018 rafaelzamora. All rights reserved.
//

import UIKit

class CustomTripCell: UITableViewCell {
    @IBOutlet weak var picture: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var conditionLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    func setTrip(trip: Trip) {
        picture.image = trip.picture
        name.text = trip.name
        location.text = trip.location
        conditionLabel.text = trip.condition
        dateLabel.text = trip.date
    }
}

class Weather {
    var temp : String
    var condition : String
    // (278K − 273.15) × 9/5 + 32
    init(temp: Double, condition: String) {
        let double = ((temp - 273.15) * (9/5) + 32)
        self.temp = String(format:"%f", double)
        self.condition = condition
    }
}
